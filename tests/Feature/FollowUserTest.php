<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class FollowUserTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    public function test_a_user_can_follow_another_user()
    {
        $user1 = User::factory()->create();
        $user2 = User::factory()->create();

        $this->actingAs($user1)
        ->post(route('follow', $user2->username));

        $this->assertTrue($user1->following($user2));

    }
}
