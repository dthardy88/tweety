<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;

class UserProfileTest extends TestCase
{
    public function test_a_user_can_update_their_profile()
    {
        $user = User::factory()->create([
            'password' => 'password'
        ]);

        $this->actingAs($user)
        ->patch(route('profile.update'), [
            'name' => 'test man',
            'username' => 'testMan1',
            'email' => 'testEmail@gmail.com',
            'tagline' => 'This is a test tagline',
            'bio' => 'This is a test bio',
            'password' => 'password',
            'password_confirmation' => 'password'
        ]);

        $user = $user->fresh();

        $this->assertEquals('test man', $user->name);
        $this->assertEquals('testMan1', $user->username);
        $this->assertEquals('testEmail@gmail.com', $user->email);
        $this->assertEquals('This is a test tagline', $user->tagline);
        $this->assertEquals('This is a test bio', $user->bio);
    }
    
    

}
