<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Tweet;

class TweetTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    public function test_a_user_can_post_a_tweet()
    {
        $user = User::factory()->create();
        $this->actingAs($user)->post('/tweets', [
            'body' => $this->faker()->text(),
        ]);
        $this->assertCount(1, $user->tweets);
    }

    public function test_a_user_can_like_a_tweet()
    {
        $user = User::factory()->create();
        $tweet = Tweet::factory()->create([
            'user_id' => $user->id,
        ]);
        $this->actingAs($user)
        ->post(route('tweet.like', $tweet->id), []);
        $this->assertCount(1, $tweet->likes);
    }

    public function test_a_user_can_delete_a_tweet()
    {
        $user = User::factory()->create();
        $tweet = Tweet::factory()->create([
            'user_id' => $user->id,
        ]);
        $this->actingAs($user)
        ->delete(route('tweet.destroy', $tweet->id), []);
        $this->assertNull($tweet->fresh());
    }

    public function test_a_user_cannot_delete_another_users_tweet()
    {
        $user1 = User::factory()->create();
        $user2 = User::factory()->create();

        $tweet = Tweet::factory()->create([
            'user_id' => $user1->id,
        ]);

        $this->actingAs($user2)
        ->delete(route('tweet.destroy', $tweet->id), []);

        $this->assertNotNull($tweet->fresh());
    }

    public function test_an_associated_like_gets_deleted_when_a_tweet_is_deleted()
    {
        $user = User::factory()->create();
        $tweet = Tweet::factory()->create([
            'user_id' => $user->id,
        ]);

        $this->actingAs($user)
        ->post(route('tweet.like', $tweet->id), []);
        $this->assertCount(1, $tweet->likes);

        $this->actingAs($user)
        ->delete(route('tweet.destroy', $tweet->id), []);
        
        $this->assertEmpty($tweet->likes()->get());

    }
}
