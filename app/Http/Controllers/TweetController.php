<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tweet;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Mentions;
use App\Notifications\UserMentionedInTweet;
use Illuminate\Notifications\Notifiable;

class TweetController extends Controller
{

    use Notifiable;

	public function __construct() 
	{
		$this->middleware('auth');
        $this->middleware('can:create,App\Models\Tweet')->only('store');
        $this->middleware('can:delete,tweet')->only('destroy');
	}

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tweets.index', [
            'tweets' => current_user()->timeline(),
            'users' => User::all()
        ]);
    }

    public function store(Request $request)
    {
    	$attributes = $request->validate([
    		'body' => 'required|max:255',
            'image' => 'file'
    	]);

    	$tweet = Tweet::create([
    		'user_id' => current_user()->id,
    		'body' => $attributes['body']
    	]);

        preg_match("/@\w\S*/", $tweet['body'], $matches);
        
        if(count($matches) > 0) {
            $username = explode('@', $matches[0]);
            $user = User::where('username', $username[1])->first();
            $tweet->mentions()->create(['user_id' => $user->id]);
            $user->notify(new UserMentionedInTweet(Auth()->user(), $tweet));
        }

        if (request()->has('image')) {
            $image['url'] = request('image')->store('tweets');
            $image['type'] = "image";
            $tweet->image()->create([
                'type' => 'image',
                'url' => $image['url'],
            ]);
        }

        $request->session()
        ->flash('flash_message', 'Tweet published');

    	return redirect('/');
    }

    public function destroy(Tweet $tweet)
    {
        $tweet->delete();
        return back()->with('flash_message', 'Tweet deleted');
    }   

}
