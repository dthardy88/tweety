<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{

    public function __construct() 
    {
        $this->middleware('auth');
        $this->middleware('can:edit,user')->only('edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $user->load('image');
        return view('profile.show', 
            [
                'user' => $user,
                'avatar' => $user->getAvatar(),
                'profile_banner' => $user->getProfileBanner(),
                'tweets' => $user->timeline()
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('profile.edit', [
            'user' => $user,
            'avatar' => $user->getAvatar(),
            'profile_banner' => $user->getProfileBanner(),
        ]);
    }

    /**
     * Update the users profile
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        $user = Auth::user();
        
        $attributes = request()->validate([
            'username' => [
                'string', 
                'required',
                'max:255', 
                Rule::unique('users')->ignore($user),
            ],
            'name' => ['string','max:255'],
            'tagline' => ['string','max:255'],
            'bio' => 'string',
            'avatar' => 'file',
            'profile_banner' => 'file',
            'email' => [
                'string',
                'required',
                'email', 
                Rule::unique('users')->ignore($user),
            ],
            'password' => [
                'string',
                'min:8', 
                'confirmed', 
                'max:255'
            ]
        ]);
        if(request()->has('avatar')) {
            $avatar['url'] = request('avatar')->store('avatars');
            $avatar['type'] = "avatar";
            $user->image()->updateOrCreate(
                ['type' => 'avatar'],
                $avatar
            );
        }
        if(request()->has('profile_banner')) {
            $profileBanner['url'] = request('profile_banner')->store('img');
            $profileBanner['type'] = "profile_banner";
            $user->image()->updateOrCreate(
                ['type' => 'profile_banner'], 
                $profileBanner
            );
        }
        $user->update([
            'username' => $attributes['username'],
            'name' => $attributes['name'],
            'password' => $attributes['password'],
            'bio' => $attributes['bio'],
            'tagline' => $attributes['tagline'],
            'email' => $attributes['email']
        ]);
        return redirect()->route('profile.edit', $user);
    }
}
