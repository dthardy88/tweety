<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;

class NotificationsController extends Controller
{
	use Notifiable;

    public function index()
    {
    	foreach(Auth()->user()->unreadNotifications as $notification) {
    		$notification->markAsRead();
    	}
    	return view('notifications.index', [
    		'notifications' => Auth()->user()->notifications
    	]);
    }
}
