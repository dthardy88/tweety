<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Notifications\Notifiable;
use App\Notifications\UserFollowed;

class FollowController extends Controller
{

    use Notifiable;

    public function __construct() 
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(User $user)
    {
        current_user()->toggleFollow($user);
        if(current_user()->following($user)) {
            $user->notify(new UserFollowed(Auth()->user()));
            return redirect(route('profile.show', $user))
                    ->with('flash_message', 'You are now following ' . $user->name);
        } else {
            return redirect(route('profile.show', $user))
                ->with('flash_message', 'User unfollowed');
        }
        
    }
}
