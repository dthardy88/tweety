<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Tweet;

class Mentions extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function tweets(): hasOne
    {
        return $this->belongsTo(Tweet::class);
    }

}
