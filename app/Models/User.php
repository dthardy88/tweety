<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Relations\hasMany;
use App\Models\Tweet;
use App\Models\Image;
use App\Traits\User\Followable;
use Illuminate\Support\Facades\Hash;
use App\Models\Like;

class User extends Authenticatable
{
    use HasFactory, Notifiable, Followable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getAvatar()
    {
        $avatar = $this->image->where('type', 'avatar')->pluck('url')->first();
        return $avatar ? asset('storage/'.$avatar) : "https://i.pravatar.cc/200?u=".$this->email;
    }

    public function getProfileBanner()
    {
        $profileBanner = $this->image->where('type', 'profile_banner')->pluck('url')->first();
        return $profileBanner ? asset('storage/'.$profileBanner) : null;
    }

    public function tweets()
    {
        return $this->hasMany(Tweet::class);
    }

    public function timeline()
    {
        $ids = $this->follows()->pluck('following_user_id');
        $ids->push($this->id);
        return Tweet::whereIn('user_id', $ids)->withLikes()->with('image')->get();
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function likes()
    {
        return $this->hasMany(Like::class);
    }

    public function image()
    {
        return $this->morphMany(Image::class, 'imageable');
    }
    
}
