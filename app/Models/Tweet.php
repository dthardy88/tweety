<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\belongsTo;
use Illuminate\Database\Eloquent\Relations\hasMany;
use Illuminate\Database\Eloquent\Relations\hasOne;
use App\Models\User;
use App\Models\Like;
use App\Models\Mentions;
use App\Models\Image;
use App\Traits\Tweet\Likeable;
use Illuminate\Notifications\Notifiable;

class Tweet extends Model
{
    use HasFactory;
    use Likeable;
    use Notifiable;

    protected $fillable = [
    	'body', 'user_id'
    ];

    public function user(): belongsTo 
    { 
    	return $this->belongsTo(User::class);
    }

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function mentions(): hasOne
    {
        return $this->hasOne(Mentions::class);
    }
 
}
