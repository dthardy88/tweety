<?php

namespace app\Traits\Tweet;

use Illuminate\Database\Eloquent\Builder;
use App\Models\Like;
use Illuminate\Database\Eloquent\Relations\hasMany;
use App\Models\User;
use App\Notifications\UserLikedTweet;

trait Likeable {

    public function scopeWithLikes(Builder $query)
    {
        $query->leftJoinSub(
            'SELECT tweet_id, sum(liked) likes, sum(!liked) dislikes from likes group by tweet_id',
            'likes',
            'likes.tweet_id',
            'tweets.id'
        );
    }

	public function like(User $user = null, User $tweetOwner)
    {
        $record = $this->likes()->where([
            ['user_id', '=' , $user->id],
        ])->get()->first();
        
        if ($record) {
            if($record->liked === 0) {
                $this->likes()->create([
                    'user_id' => $user->id,
                    'liked' => 1,
                ]);
            }
            $record->delete();
        } else {
            if($tweetOwner->id !== Auth()->user()->id) {
                $tweetOwner->notify(new UserLikedTweet(Auth()->user()));
            }
            $this->likes()->create([
                'user_id' => $user->id,
                'liked' => 1,
            ]);
        }
    }

    public function dislike(User $user = null)
    {
    	$record = $this->likes()->where([
            ['user_id', '=' , $user->id],
        ])->get()->first();
        
        if ($record) {
            if($record->liked === 1) {
                $this->likes()->create([
                    'user_id' => $user->id,
                    'liked' => 0,
                ]);
            }
            $record->delete();
        } else {
            $this->likes()->create([
                'user_id' => $user->id,
                'liked' => 0,
            ]);
        }

    }

    public function likes(): hasMany
    {
    	return $this->hasMany(Like::class);
    }

    public function isLikedByUser(User $user)
    { 
       return (bool) $user->likes()->where([
            ['tweet_id', '=', $this->id],
            ['liked', '=', 1]
       ])->exists();
    }

    public function isDislikedByUser(User $user)
    { 
        return (bool) $user->likes()->where([
            ['tweet_id', '=', $this->id],
            ['liked', '=', 0]
        ])->exists();
    }

}