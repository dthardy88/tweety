<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TweetController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\FollowController;
use App\Http\Controllers\ExploreController;
use App\Http\Controllers\TweetLikesController;
use App\Http\Controllers\NotificationsController;
use App\Models\User;
use App\Models\Tweet;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [TweetController::class, 'index'])
->name('home');

Route::get('/profile/{user:username}', [ProfileController::class, 'show'])
->name('profile.show');

Route::patch('/profile/update', [ProfileController::class, 'update'])
->name('profile.update');

Route::get('/profile/{user:username}/edit', [ProfileController::class, 'edit'])
->name('profile.edit');

Route::post('/profile/{user:username}/follow', [FollowController::class, 'store'])
->name('follow');

Route::post('/tweets', [TweetController::class, 'store'])
->name('tweets.store');

Route::delete('/tweet/delete/{tweet}', [TweetController::class, 'destroy'])
->name('tweet.destroy');

Route::post('/tweets/{tweet}/like', [TweetLikesController::class, 'store'])
->name('tweet.like');

Route::delete('/tweets/{tweet}/dislike', [TweetLikesController::class, 'destroy'])
->name('tweet.dislike');

Route::get('/explore', ExploreController::class)
->name('explore');

Route::get('/notifications', [NotificationsController::class, 'index'])
->name('notifications');

require __DIR__.'/auth.php';
