<div class="flex p-4 relative">
    <div class="delete-icon absolute right-0 text-lg top-0 p-4">
        <form action="{{route('tweet.destroy', $tweet)}}" method="POST">
            @csrf
            @method('DELETE')
            @can('delete', $tweet)
                <button type="submit" title="Delete tweet">X</button>
            @endcan
        </form>
    </div>
    <div class="mr-2 flex-shrink-0">
        <a href="{{route('profile.show', $tweet->user)}}">
            <img 
                src="{{$tweet->user->getAvatar()}}" 
                alt="Avatar" class="rounded-full mr-2 object-cover h-11 w-11"
                width="40"
                height="40">
        </a>
    </div>
    <div>
        <h5 class="font-bold mb-4">
            <a href="{{route('profile.show', $tweet->user)}}">
                {{$tweet->user->name}}
            </a>
        </h5>
        <p class="text-sm">
            {{$tweet->body}}
        </p>
        @if($tweet->image)
            <div class="tweet_image py-4">
                <img 
                    src="{{asset('storage/'.$tweet->image->url)}}" 
                    alt="{{$tweet->body}}" 
                    class="rounded-lg object-cover" />
            </div>
        @endif
        <x-like-dislike-buttons :tweet="$tweet">
        </x-like-dislike-buttons>
    </div>
</div>