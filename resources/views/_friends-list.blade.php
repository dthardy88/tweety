<div class="border-grey-300 bg-blue-100 rounded-lg p-4 mt-8 lg:mt-0">
	<h3 class="pb-4 font-bold">Following</h3>
	<ul>
		@forelse (Auth()->user()->follows as $user)
			<li class="{{$loop->last ? '' : 'pb-4'}}">
				<div class="text-sm">
					<a 
						href="{{route('profile.show', $user)}}" class="xl:flex items-center">
						<img 
							src="{{$user->getAvatar()}}" 
							alt="Avatar" 
							class="rounded-full mr-2 mb-2" 
							width="40"
							height="40"
							style="height:40px;"
						/>
						{{$user->name}}
					</a>
				</div>
			</li>
			@empty
				<li class="text-sm">You have no friends yet, go make some!</li>
		@endforelse
	</ul>
</div>