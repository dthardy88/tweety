<div class="border border-blue-400 rounded-lg px-8 pt-6 pb-4 mb-8 relative">
    <form
        id="publish-tweet"
        action="{{route('tweets.store')}}" 
        method="POST" 
        enctype="multipart/form-data">
        @csrf
        <div 
            id="user-mentions" 
            class="hidden w-60 border-2 h-auto p-4 bg-white border-1 border-grey-500 bg-blue-100 rounded-lg absolute overflow-y-scroll top-2 left-2 z-50">
            <ul>
                @foreach($users as $user)
                    <li>
                        <a 
                            href="" 
                            class="text-blue-700 hover:underline user-link"
                            data-userid="{{$user->id}}"
                            onClick="setMentionInTweet(event)">
                            {{'@'.$user->username}}
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
        <div 
            id="tweet-content"
            class="textarea outline-none break-all"
            style="min-height:7rem"
            contenteditable="true">
        </div>
        <textarea
            id="tweet-textarea-hidden" 
            name="body" 
            class="hidden" 
            required
            maxlength="255" 
            autofocus></textarea>
        <hr class="my-4" />
        <div class="sm:flex items-center justify-between">
            <div 
                class="pb-4 sm:pb-0 mr-2" 
                style="min-width:2.75rem;">
                <img 
                    src="{{Auth()->user()->getAvatar()}}" 
                    alt="Avatar" 
                    class="rounded-full mr-2 h-11 w-11"
                />
            </div>
            <div class="pb-6 sm:pb-0">
                <x-label 
                    for="image" 
                    :value="__('Attach Image')" 
                    class="pb-2" />
                <input type="file" name="image">
            </div>
            <div>
                <button 
                    id="tweet-submit"
                    type="submit" 
                    class="bg-blue-500 hover:bg-blue-400 rounded-full shadow px-8 text-white h-10 text-sm whitespace-nowrap">Tweet-a-roo!
                </button>
            </div>
        </div>
        @error('body')
            <p class="text-red-600 text-sm mt-4">{{$message}}</p>
        @enderror
    </form>
    <div id="remaining-characters" class="text-right pt-2">
        <span class="text-sm">242</span>
    </div>
</div>