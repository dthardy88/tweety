@extends('layouts.app')
@section('content')
    <div>
        @foreach($users as $user)
            <div class="flex items-center mb-4">
                <div>
                    <img
                        src="{{$user->getAvatar()}}"
                        alt="{{$user->username}}"
                        width="60"
                        height="60"
                        class="rounded-lg w-16 h-16 object-cover"
                    />
                </div>
                <div>
                    <a href="{{route('profile.show', $user)}}">
                        <h4 
                            class="font-bold pl-2">
                            {{'@'.$user->username}}
                        </h4>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
@endsection