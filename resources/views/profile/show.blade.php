@extends('layouts.app')
@section('content')
	<header class="mb-6">
		<div class="relative">
			@if($profile_banner)
				<img 
					src="{{$profile_banner}}" 
					alt="Profile banner"
					class="mb-3 rounded-lg object-cover h-60 w-full object-center object-cover"
				/>
			@else
				<div 
					class="default_banner rounded-lg" 
					style="background-color:#E8E8E8; height:230px;">
				</div>
			@endif
			<img 
				src="{{$user->getAvatar()}}" 
	            alt="Avatar" 
	            class="rounded-full mr-2 absolute bottom-0 transform -translate-x-1/2 translate-y-1/2 h-32 w-32 object-cover"
	            style="left: 50%"
	            width="125" />
		</div>
		<div class="flex justify-between items-center mb-4">
			<div>
				<h5 class="mt-16 sm:mt-0 font-bold text-2xl w-40 leading-none pb-4 pt-2">
					{{$user->name}}
				</h5>
				<p class="text-sm">{{$user->tagline}}</p>
			</div>
			<div class="flex mt-12 sm:mt-0">
				@can('edit', $user)
					<a href="{{route('profile.edit', $user)}}" class="rounded-full border border-gray-200 py-2 px-2 text-black text-xs mr-2">Edit Profile</a>
				@endcan
				<x-follow-button :user="$user">
				</x-follow-button>
			</div>
		</div>
        <p class="text-sm">{{$user->bio}}</p>
	</header>
	@include('_timeline')
@endsection