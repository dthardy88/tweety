@extends('layouts.app')
@section('content')
	<!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />
        <form 
            method="POST" 
            action="{{ route('profile.update') }}" 
            enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <h2 class="text-3xl mb-4">Profile</h2>
            <div class="profile_banner ">
                <x-label 
                    for="profile_banner" 
                    :value="__('Profile Banner')" 
                    class="pb-1" />
                <input type="file" name="profile_banner" />
                @if($profile_banner)
                    <img 
                        src="{{$profile_banner}}" 
                        alt="{{$user->name}} profile banner"
                        class="mt-4 rounded-lg object-cover h-60 w-full object-center">
                @endif
             </div>
             <div class="tagline">
                <x-label 
                    for="tagline" 
                    :value="__('Tagline')"
                    class="my-4" />
                <x-input 
                    id="tagline" 
                    class="block mt-1 w-full" 
                    type="text" 
                    name="tagline" 
                    :value="$user->tagline" 
                    autofocus />
             </div>
             <x-label 
                for="bio" 
                :value="__('Bio')"
                class="my-4" />
             <div class="bio p-4 border border-grey-300 shadow-sm rounded-lg mb-8">
                <textarea 
                    name="bio"
                    class="w-full border-0 focus:ring-0 p-0 h-32" 
                    placeholder="Enter your bio"
                    required
                    autofocus>{{$user->bio}}</textarea>
             </div>
             <hr />
             <h2 class="text-3xl my-4">Account</h2>
            <!-- Username -->
            <div class="mt-4">
                <x-label for="username" :value="__('Username')" />
                <x-input id="username" class="block mt-1 w-full" type="text" name="username" :value="$user->username" required autofocus />
            </div>
            <!-- Name -->
            <div class="mt-4">
                <x-label for="name" :value="__('Name')" />
                <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="$user->name" required autofocus />
            </div>
            <!-- Email Address -->
            <div class="mt-4">
                <x-label for="email" :value="__('Email')" />
                <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="$user->email" required />
            </div>
            <!-- Avatar -->
            <div class="mt-4 flex">
                @if($avatar)
                    <div class="pr-4">
                        <img 
                            src="{{$user->getAvatar()}}"
                            class="rounded-full"
                            width="40"
                            height="40"
                        />
                    </div>
                @endif
                <div>
                    <x-label for="avatar" :value="__('Avatar')" />
                    <x-input 
                        id="avatar" 
                        class="block mt-1 w-full" 
                        type="file" 
                        name="avatar" 
                    />
                </div>
            </div>
            <!-- Password -->
            <div class="mt-4">
                <x-label for="password" :value="__('Password')" />
                <x-input 
                	id="password" 
                	class="block mt-1 w-full"
                    type="password"
                    name="password"
                    autocomplete="new-password" />
            </div>
            <!-- Confirm Password -->
            <div class="mt-4">
                <x-label for="password_confirmation" :value="__('Confirm Password')" />
                <x-input 
                	id="password_confirmation" 
                	class="block mt-1 w-full"
                    type="password"
                    name="password_confirmation"
                />
            </div>
            <x-button class="mt-4">
            	{{ __('Update') }}
            </x-button>
            <a 
                href="{{route('profile.show', $user)}}" 
                class="hover:underline pl-4">
                Cancel
            </a>
        </form>
@endsection