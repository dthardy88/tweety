<ul>
	<li>
		<a 
			class="font-bold text-lg mb-4 block" 
			href="{{route('home')}}">
			Home
		</a>
	</li>
	<li>
		<a 
			class="font-bold text-lg mb-4 block" 
			href="{{route('explore')}}">
			Explore
		</a>
	</li>
	<li>
		<a 
			class="font-bold text-lg mb-4 block" 
			href="{{route('profile.show', Auth()->user())}}">
			Profile
		</a>
	</li>
	<li>
		<a 
			class="font-bold flex text-lg mb-4 block" 
			href="{{route('notifications', Auth()->user())}}">
			Notifications
			@if(count(Auth()->user()->unreadNotifications()->get()))
				<img 
					src="{{asset('storage/img/notification-icon.webp')}}" 
					width="25"
					height="25"
					alt="Notification icon"
					class="ml-4" />
			@endif
		</a>
	</li>
</ul>