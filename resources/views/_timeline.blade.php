@if(count($tweets) > 0)
	@foreach($tweets as $tweet)
		<div class="border border-grey-300 rounded-lg">
			@include('_tweet')	
		</div>
	@endforeach
@else
	<p 
		class="text-sm p-4 rounded-lg border border-grey-300">
		No tweets to show!
	</p>
@endif