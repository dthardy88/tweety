<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        
    </head>
    <body class="font-sans antialiased">
        <div>
            <!-- Page Heading -->
            <header class="bg-white shadow">
                <div class="container mx-auto">
                    @include('layouts.navigation')
                </div>
            </header>
            <main class="px-12 max-w-7xl mx-auto">
                <div class="container mx-auto">
                    <div class="py-12">
                        <div class="lg:flex lg:justify-between">
                            <div class="lg:w-1/6">
                                @include('_sidebar-links')
                            </div>
                            <div class="lg:flex-1 lg:mx-10">
                                @yield('content')
                            </div>
                            <div class="lg:w-1/6">
                                @include('_friends-list')
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </body>
    <footer class="relative px-12 pt-40 pb-60">
        @if(Session::has('flash_message'))
            <div id="flash-message" class="absolute right-0 mr-40 bg-blue-500 text-white px-12 py-4 rounded-lg">{{Session::get('flash_message')}}</div>
        @endif
    </footer>
</html>
