@extends('layouts.app')
@section('content')
	<ul>
	    @if($notifications->count())
		    @foreach($notifications as $notification)
		    	<li class="p-4 border-2 border-blue-100">
		    		{{'@'.$notification['data']['message']}}
		    	</li>
		    @endforeach
	    @else
	    	<p>No notifications to show</p>
	    @endif
   </ul>
@endsection