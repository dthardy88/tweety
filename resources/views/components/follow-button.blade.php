@if(current_user()->isNot($user))
	<form method="POST" action="{{route('follow', $user)}}">
		@csrf
		<button type="submit" href="{{route('follow', $user->username)}}" class="bg-blue-500 rounded-full shadow py-2 px-2 text-white text-xs">
			{{Auth()->user()->following($user) ? 'Unfollow Me' : 'Follow Me'}}
		</a>
	</form>
@endif