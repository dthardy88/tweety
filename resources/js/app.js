"use strict";

require('./bootstrap');
require('alpinejs');

window.addEventListener('DOMContentLoaded', (event) => {

	const tweetForm = document.getElementById('publish-tweet');
	const tweetField = document.getElementById('tweet-content');
	let mentionWindowOpen = false;
	const mentionModal = document.getElementById('user-mentions');
	const pattern = /@\w/

	const msg = document.getElementById('flash-message');
	if(msg) {
		setTimeout(() => {
			msg.style.opacity = "0";
		}, 3000);
	}

	if(tweetField) {
		tweetField.addEventListener('input', (event) => {
			event.preventDefault();
			const string = event.target.innerText;
			calculateRemainingChar(string);
			listenForMentionInTweet(string);
		});
	}
	
	const btnSubmit = document.getElementById('tweet-submit');
	if(btnSubmit) {
		btnSubmit.addEventListener('click', (event) => {
			event.preventDefault();
			let tweetContent = document.getElementById('tweet-content');
			let textarea = document.getElementById('tweet-textarea-hidden');
			textarea.value = tweetContent.innerText;
			tweetForm.submit();
		});
	}

	window.calculateRemainingChar = (string) => {
	    const maxChars = 255;
	    let stringLength = string.length;
	    let remainingChar = (maxChars - stringLength);
	    const numDisplayed = document.getElementById('remaining-characters');
	    numDisplayed.innerText = remainingChar;
	}

	window.listenForMentionInTweet = (str) => {
		let endOfString = str.substr(str.length -3);
		let mentionFound = endOfString.search(pattern);
		if(mentionFound !== -1) {
			mentionModal.classList.remove('hidden');
			mentionWindowOpen = true;
		}
	}

	window.addEventListener('click', (event) => {
		if(mentionWindowOpen === true) {
			event.preventDefault();
			if(!event.target.classList.contains('user-link')) {
				mentionModal.classList.add('hidden');
			}
		}
	});

	window.setMentionInTweet = (event) => {
		let newTweetContent = tweetField.innerHTML.replace(pattern, event.target.innerHTML);
		tweetField.innerHTML = newTweetContent;
		mentionModal.classList.add('hidden');
	}

});


